package sample;

import backend.game.models.EGameLevel;
import backend.game.models.Game;
import backend.player.models.computer.ComputerPlayer;
import backend.stone.models.EStoneType;
import backend.strategy.informed.AStarStrategy;
import backend.strategy.uninformed.BFSStrategy;
import backend.strategy.uninformed.DFSStrategy;
import backend.strategy.uninformed.UniformCostSearch;
import javafx.application.Application;

import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        MainView mainView = new MainView();
//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(mainView, 640, 480));
        primaryStage.show();
    }

    public static void main(String[] args) {
        // Init player
        ComputerPlayer player = new ComputerPlayer();
        // Init player strategy
        player.initStrategy(new UniformCostSearch());
        // Init game
        Game.initGame(EGameLevel.EASY_LEVEL, player, EStoneType.HORSE);
        // Play game
        player.play();
    }
}