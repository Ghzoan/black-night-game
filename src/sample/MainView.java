package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class MainView extends VBox {
    private Button button;
    private Canvas canvas;

    public MainView(){
        this.button = new Button();
        this.button.setText("Hello");
        this.canvas = new Canvas();
        this.getChildren().addAll(button, canvas);
    }
}
