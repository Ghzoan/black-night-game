package backend.strategy;

import backend.game.models.Game;
import backend.ground.board.models.Board;
import backend.ground.board.services.BoardService;
import backend.ground.cell.models.Cell;

import java.util.LinkedList;
import java.util.List;

/** This class will be define strategy of computer player
 *
 * @author Ghzoan Safi
 **/
public abstract class Strategy {

    /** Path to end state **/
    protected LinkedList<Board> visitedBoardsList = new LinkedList<>();

    /**
     * Check if board visited before
     *
     * @param board board
     *
     * @return boolean
     **/
    protected boolean checkIfBoardVisited(Board board){
        for (Board visitedBoard: this.visitedBoardsList
        ) {
            if (visitedBoard.equals(board)){
                return true;
            }
        }
        return false;
    }

    /** Print results
     *
     * @return void
     **/
    public void printResults(){
        List<Board> listForPrint = Game.getGame().getGameBoard().pathToBoard;

        System.out.println("Path to end state: " );
        for (Board board: listForPrint){
            Cell stoneCell = (new BoardService()).getCellOfStone(board);
            System.out.println(stoneCell);
        }
        System.out.println(" ----------------------------- ");
        System.out.println("Count of states: " + listForPrint.size());
        System.out.println("Count of visited nodes: " + this.visitedBoardsList.size());
    }

    /**
     * Clone path
     *
     * @param board Board
     *
     * @return List<Board>
     **/
    protected List<Board> clonePath(Board board){
        LinkedList<Board> newPath = new LinkedList<>();
        newPath.addAll(board.pathToBoard);
        newPath.add(board);
        return newPath;
    }


    /**
     * Apply logic of strategy on game
     *
     * @return void
     **/
    public abstract void applyStrategy();
}
