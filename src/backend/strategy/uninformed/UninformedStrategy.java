package backend.strategy.uninformed;

import backend.ground.board.models.Board;
import backend.ground.board.services.BoardService;
import backend.ground.cell.models.Cell;
import backend.strategy.Strategy;

import java.util.LinkedList;

/** This class will be define uninformed search strategy of computer player
 *
 * @author Ghzoan Safi
 **/
public abstract class UninformedStrategy extends Strategy {
}
