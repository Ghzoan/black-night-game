package backend.strategy.uninformed;

import backend.game.models.Game;
import backend.ground.board.models.Board;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/** This class will be define BFS strategy of computer player
 *
 * @author Ghzoan Safi
 **/
public class BFSStrategy extends UninformedStrategy {

    /** Queue of strategy **/
    protected Queue<Board> boardsQueue;

    /**
     * Class constructor
     *
     **/
    public BFSStrategy(){
        // Initialize queue
        this.boardsQueue = new LinkedList<>();
    }

    /**
     * Apply logic of strategy on game
     *
     * @return void
     **/
    @Override
    public void applyStrategy() {
        // Add initial state to queue
        this.boardsQueue.add(Game.getGame().getGameBoard());
        // Loop on queue
        while (!this.boardsQueue.isEmpty() && !Game.getGame().checkPlayerWin()){
            // Get board from queue
            Board board = this.boardsQueue.remove();
            // Move game to this board
            Game.getGame().applyPlayerMove(board);
            // Get available state for this board
            LinkedList<Board> availableBoards = Game.getGame().getAvailableMovements();
            // Add all available boards to queue
            for (Board availableBoard: availableBoards){
                // Check if this board visited before
                if (!this.checkIfBoardVisited(availableBoard)){
                    // Add board to queue
                    this.boardsQueue.add(availableBoard);
                    // Add to path
                    availableBoard.pathToBoard = this.clonePath(board);
                }
            }
            // Add board to visited list
            this.visitedBoardsList.add(board);
        }
        // Print results
        this.printResults();
    }

}
