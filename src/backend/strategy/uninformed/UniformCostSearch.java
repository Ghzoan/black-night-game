package backend.strategy.uninformed;

import backend.ground.board.models.Board;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

/** This class will be define Uniform Cost Search strategy of computer player.
 *
 * @author Ghzoan Safi
 **/
public class UniformCostSearch extends BFSStrategy{

    /**
     * Class constructor
     *
     **/
   public UniformCostSearch(){
        // Initialize queue
       // Its same of bfs because cost of move is constant
        this.boardsQueue = new LinkedList<>();
   }
}
