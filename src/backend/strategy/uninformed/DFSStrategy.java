package backend.strategy.uninformed;


import backend.game.models.Game;
import backend.ground.board.models.Board;

import java.util.LinkedList;
import java.util.Stack;

/** This class will be define DFS strategy of computer player
 *
 * @author Ghzoan Safi
 **/
public class DFSStrategy extends UninformedStrategy {

    /** Stack of boards **/
    Stack<Board> boardsStack = new Stack<>();

    /**
     * Apply logic of strategy on game
     *
     * @return void
     **/
    @Override
    public void applyStrategy() {
        // Add initial state to stack
        this.boardsStack.push(Game.getGame().getGameBoard());
        // Loop on stack
        while (!boardsStack.empty() && !Game.getGame().checkPlayerWin()){
            // Pop board from stack
            Board board = this.boardsStack.pop();
            // Move game to this board
            Game.getGame().applyPlayerMove(board);
            // Get available state for this board
            LinkedList<Board> availableBoards = Game.getGame().getAvailableMovements();
            for (Board availableBoard: availableBoards){
                // Check if this board visited before
                if (!this.checkIfBoardVisited(availableBoard)){
                    // Push board to queue
                    this.boardsStack.push(availableBoard);
                    // Add to path
                    availableBoard.pathToBoard = this.clonePath(board);
                }
            }
            // Add board to visited list
            this.visitedBoardsList.add(board);
        }

        this.printResults();
    }
}
