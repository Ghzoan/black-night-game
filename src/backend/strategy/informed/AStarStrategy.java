package backend.strategy.informed;

import backend.ground.board.models.Board;
import backend.strategy.uninformed.UniformCostSearch;

import java.util.Comparator;
import java.util.PriorityQueue;

/** This class will be define A* strategy of computer player
 *
 * @author Ghzoan Safi
 **/
public class AStarStrategy extends UniformCostSearch {

    int moveCost = 3;

    /**
     * Class constructor
     *
     **/
    public AStarStrategy(){
        // Initialize queue
        this.boardsQueue = new PriorityQueue<>(new Comparator<Board>() {
            @Override
            public int compare(Board board1, Board board2) {
                int firstBoardValue = board1.distanceBetweenStoneAndGoal();
                int secondBoardValue = board2.distanceBetweenStoneAndGoal();
                if (firstBoardValue < secondBoardValue && firstBoardValue >= moveCost){
                    return -1;
                }
                else if (firstBoardValue > secondBoardValue && secondBoardValue >= moveCost){
                    return 1;
                }
                return 0;
            }
        });
    }
}
