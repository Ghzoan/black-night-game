package backend.strategy.informed;

import backend.strategy.Strategy;


/** This class will be define informed search strategy of computer player
 *
 * @author Ghzoan Safi
 **/
public abstract class InformedStrategy extends Strategy {

}
