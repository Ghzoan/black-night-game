package backend.stone.models;

/**
 * This class will be define each stone can be move on game board
 *
 * @author Ghzoan Safi
**/
public abstract class Stone {
    /**
     * Define move for stone from current position to another one, this will be an abstract method, each child must be declare this method.
     *
     * @param oldXPosition int
     * @param oldYPosition int
     * @param newXPosition int
     * @param newYPosition int
     *
     * @return boolean
     **/
    public abstract boolean canMoveTo(int oldXPosition, int oldYPosition, int newXPosition, int newYPosition);
}
