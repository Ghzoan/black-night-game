package backend.stone.models;

public class HorseStone extends Stone {

    /**
     * Define move for stone from current position to another one, this will be an abstract method, each child must be declare this method.
     *
     * @param oldXPosition int
     * @param oldYPosition int
     * @param newXPosition int
     * @param newYPosition int
     *
     * @return boolean
     **/
    @Override
    public boolean canMoveTo(int oldXPosition, int oldYPosition, int newXPosition, int newYPosition) {
        return oldXPosition != newXPosition
                && oldYPosition != newYPosition
                && ( (Math.abs(oldXPosition - newXPosition) == 2 && Math.abs(oldYPosition -  newYPosition) == 1) ||
                (Math.abs(oldXPosition - newXPosition) == 1 && Math.abs(oldYPosition - newYPosition) == 2));
    }
}
