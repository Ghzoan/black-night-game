package backend.stone.services;

import backend.stone.models.*;

/** This class created for init stone types
 *
 * @author Ghzoan Safi
 **/
public class StoneFactory {

    /**
     * Init stone by specific type
     *
     * @param stoneType EStoneType
     *
     * @return Stone
     **/
    public Stone initStone(EStoneType stoneType){
        if (stoneType == EStoneType.HORSE){
            return new HorseStone();
        }
        return null;
    }
}
