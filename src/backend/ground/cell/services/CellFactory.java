package backend.ground.cell.services;

import backend.ground.cell.models.*;


/** This class created for init cell types
 *
 * @author Ghzoan Safi
 **/
public class CellFactory {

    /**
     * Init cell by specific type
     *
     * @param cellType ECellType
     * @param xIndex int
     * @param yIndex int
     * @param haveGoal boolean
     *
     * @return Cell
     **/
    public Cell initCell(ECellType cellType, int xIndex, int yIndex, boolean haveGoal){
        if (cellType == ECellType.AVAILABLE_CELL){
            return new AvailableCell(xIndex, yIndex, haveGoal);
        }
        return new BlockedCell(xIndex, yIndex);
    }
}
