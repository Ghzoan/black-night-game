package backend.ground.cell.services;

import backend.ground.cell.models.*;
import backend.shared.service.ConsoleColor;

public class CellService {

    /**
     * Get cell type
     *
     * @param cell Cell
     *
     * @return ECellType
     **/
    public ECellType getCellType(Cell cell){
        if (cell instanceof AvailableCell){
            return ECellType.AVAILABLE_CELL;
        }
        return ECellType.BLOCKED_CELL;
    }

    /**
     * Get color of cell for console view
     *
     * @param cell Cell
     *
     * @return String
     **/
    public String getColorForConsoleView(Cell cell){
        if (cell.isHaveGoal() && cell.isHaveStone()){
            return ConsoleColor.ANSI_GREEN;
        }
        if (cell.isHaveGoal()){
            return ConsoleColor.ANSI_BLUE;
        }
        if (cell.isHaveStone()){
            return ConsoleColor.ANSI_PURPLE;
        }
        return ConsoleColor.ANSI_RESET;
    }
}
