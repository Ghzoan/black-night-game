package backend.ground.cell.models;

/** This class will be define each blocked cell inside game board
 *
 * @author Ghzoan Safi
 **/
public class BlockedCell extends Cell{
    /**
     * Class constructor
     *
     * @param xIndex int
     * @param yIndex int
     **/
    public BlockedCell(int xIndex, int yIndex) {
        super(xIndex, yIndex, false);
    }
}
