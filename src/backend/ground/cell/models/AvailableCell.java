package backend.ground.cell.models;

/** This class will be define each available cell inside game board
 *
 * @author Ghzoan Safi
 **/

public class AvailableCell extends Cell {

    /**
     * Class constructor
     *
     * @param xIndex int
     * @param yIndex int
     * @param haveGoal boolean
     **/
    public AvailableCell(int xIndex, int yIndex, boolean haveGoal) {
        super(xIndex, yIndex, haveGoal);
    }
}
