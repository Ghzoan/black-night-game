package backend.ground.cell.models;

import backend.ground.cell.services.CellService;
import backend.shared.service.ConsoleColor;

/** This class will be define each cell inside game board
 *
 * @author Ghzoan Safi
 **/
public abstract class Cell implements Cloneable{
    /** Index this cell on x in board **/
    private int xIndex;

    /** Index this cell on y in board **/
    private int yIndex;

    /** If cell have stone on it **/
    private boolean haveStone;

    /** If cell have goal on it**/
    private boolean haveGoal;

    /** Cell service instance **/
    protected CellService _cellService;

    /**
     * Class constructor
     *
     * @param  xIndex int
     * @param  yIndex int
     * @param  haveGoal boolean
     *
     **/
    public Cell(int xIndex, int yIndex, boolean haveGoal){
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        this.haveGoal = haveGoal;
        this._cellService = new CellService();
    }

    /**
     * Get x index for cell
     *
     * @return int
     **/
    public int getXIndex() {
        return xIndex;
    }

    /**
     * Set x index for cell
     *
     * @param xIndex int
     *
     * @return void
     **/
    public void setXIndex(int xIndex) {
        this.xIndex = xIndex;
    }

    /**
     * Get y index for cell
     *
     * @return int
     **/
    public int getYIndex() {
        return yIndex;
    }

    /**
     * Set y index for cell
     *
     * @param yIndex int
     *
     * @return void
     **/
    public void setYIndex(int yIndex) {
        this.yIndex = yIndex;
    }

    /**
     * Get have stone attribute
     *
     * @return boolean
     **/
    public boolean isHaveStone() {
        return haveStone;
    }

    /**
     * Set have stone attribute
     *
     * @param haveStone boolean
     *
     * @return void
     **/
    public void setHaveStone(boolean haveStone) {
        this.haveStone = haveStone;
    }

    /**
     * Get have goal attribute
     *
     * @return boolean
     **/
    public boolean isHaveGoal() {
        return haveGoal;
    }

    /**
     * Set have goal attribute
     *
     * @param haveGoal boolean
     *
     * @return void
     **/
    public void setHaveGoal(boolean haveGoal) {
        this.haveGoal = haveGoal;
    }


    /**
     * Clone method
     *
     * @return Object
     **/
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * Implementation to string method for this class
     *
     * @return String
     **/
    @Override
    public String toString() {
        return this._cellService.getColorForConsoleView(this) + "Cell{" +
                "xIndex=" + xIndex +
                ", yIndex=" + yIndex +
                ", haveStone=" + haveStone +
                ", haveGoal=" + haveGoal +
                '}' + ConsoleColor.ANSI_RESET;
    }
}
