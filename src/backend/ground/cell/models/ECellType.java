package backend.ground.cell.models;

/** This enum to describe all types of cell
 *
 * @author Ghzoan Safi
 **/
public enum ECellType {
    AVAILABLE_CELL,
    BLOCKED_CELL
}
