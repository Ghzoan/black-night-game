package backend.ground.board.services;

import backend.game.models.EGameLevel;
import backend.ground.board.models.BoardInitializer;
import backend.ground.board.models.EasyLevelBoardInitializer;

/** This class created for init initializer types
 *
 * @author Ghzoan Safi
 **/
public class BoardInitializerFactory {

    /**
     * Init board initializer by game level
     *
     * @param gameLevel EGameLevel
     *
     * @return BoardInitializer
     **/
    public BoardInitializer initBoardInitializer(EGameLevel gameLevel){
        if (gameLevel == EGameLevel.EASY_LEVEL){
            return new EasyLevelBoardInitializer();
        }
        return null;
    }
}
