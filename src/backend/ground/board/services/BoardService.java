package backend.ground.board.services;

import backend.ground.board.models.Board;
import backend.ground.cell.models.Cell;

/** This class will be define methods applied on board
 *
 * @author Ghzoan Safi
 **/
public class BoardService {

    /**
     * Get current cell of player
     *
     * @param board Board
     *
     * @return Cell
     **/
    public Cell getCellOfStone(Board board){
        Cell target = null;
        for (Cell cell : board.getCells()){
            if (cell.isHaveStone()) {
                target = cell;
            }
        }
        return target;
    }


    /**
     * Get cell of goal
     *
     * @param board Board
     *
     * @return Cell
     **/
    public Cell getCellOfGoal(Board board){
        Cell target = null;
        for (Cell cell : board.getCells()){
            if (cell.isHaveGoal()) {
                target = cell;
            }
        }
        return target;
    }

    /**
     * Check if player win game
     *
     * @param board Board
     *
     * @return boolean
     **/
    public boolean checkIfPlayerWin(Board board){
        Cell target = null;
        for (Cell cell : board.getCells()){
            if (cell.isHaveStone() && cell.isHaveGoal()) {
                target = cell;
            }
        }
        return target != null;
    }

    /**
     * Get distance between tow cell by manhattan distance
     *
     * @param srcCell
     * @param targetCell
     *
     * @return int**/
    public int distanceBetweenTowCells(Cell srcCell, Cell targetCell){
        return Math.abs((srcCell.getXIndex() - targetCell.getXIndex())) + Math.abs((srcCell.getYIndex() - targetCell.getYIndex()));
    }

    /**
     * Get cell from board by index
     *
     * @param board Board
     * @param xIndex int
     * @param yIndex int
     *
     * @return Cell
     **/
    public Cell getCellByIndexes(Board board, int xIndex, int yIndex){
        return board.getCells().stream()
                .filter(cell -> cell.getXIndex() == xIndex && cell.getYIndex() == yIndex)
                .findAny()
                .orElse(null);
    }
}
