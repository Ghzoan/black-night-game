package backend.ground.board.models;

import backend.ground.cell.models.Cell;
import backend.ground.cell.services.CellFactory;

import java.util.List;

/** This class created for initialize the board of game depend on game level
 *
 * @author Ghzoan safi
 **/
public abstract class BoardInitializer {

    /** Width of board **/
    protected int boardWidth;

    /** Height of board  **/
    protected int boardHeight;

    /** Cell factory instance **/
    protected CellFactory cellFactory;

    /**
     * Class constructor
     *
     **/
    public BoardInitializer(){
        this.cellFactory = new CellFactory();
    }

    /**
     * Initialize board method, this will be an abstract method, each child must be declare this method.
     *
     *
     * @return Board
     **/
    public abstract Board Initialize();
}
