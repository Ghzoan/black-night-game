package backend.ground.board.models;


import backend.game.models.Game;
import backend.ground.board.services.BoardService;
import backend.ground.cell.models.Cell;
import backend.player.models.computer.ComputerPlayer;
import backend.strategy.uninformed.DFSStrategy;

import java.util.LinkedList;
import java.util.List;

/** This class will be define board of game
 *
 * @author Ghzoan Safi
**/
public class Board implements Cloneable{

    /** Width of board **/
    private int width;

    /** Height of board **/
    private int height;

    /** List of cells **/
    private List<Cell> cells;

    /** Path to board **/
    public List<Board> pathToBoard;


    public BoardService boardService = new BoardService();

    /**
     * Class Constructor
     *
     **/
    public Board(){
        this.pathToBoard = new LinkedList<>();
    }

    /**
     * Get width of board
     *
     * @return int
     **/
    public int getWidth() {
        return width;
    }

    /**
     * Set width of board
     *
     * @param width int
     *
     * @return void
     **/
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Get height of board
     *
     * @return int
     **/
    public int getHeight() {
        return height;
    }

    /**
     * Set height of board
     *
     * @param height int
     *
     * @return void
     **/
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Get cells of board
     *
     * @return List<Cell>
     **/
    public List<Cell> getCells() {
        return cells;
    }

    /**
     * Set cells of board
     *
     * @param cells List<Cell>
     *
     * @return void
     **/
    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    /**
     * Move stone to
     *
     * @param xIndex int
     * @param yIndex int
     *
     * @return void
     **/
    public void moveStoneTo(int xIndex, int yIndex){
        for (Cell cell: this.cells
             ) {
            if (cell.isHaveStone() && xIndex != cell.getXIndex() && yIndex != cell.getYIndex()){
                cell.setHaveStone(false);
            }

            if (xIndex == cell.getXIndex() && yIndex == cell.getYIndex()){
                cell.setHaveStone(true);
            }
        }
    }

    /**
     * Get distance between stone and goal
     *
     * @return int
     **/
    public int distanceBetweenStoneAndGoal(){
        Cell stoneCell = this.boardService.getCellOfStone(this);
        Cell goalCell = this.boardService.getCellOfGoal(this);
        return this.boardService.distanceBetweenTowCells(stoneCell, goalCell);
    }

    /**
     * Cost between to cell
     *
     * return int
     **/
    public int costBetweenTowCell(){
        return 0;
    }

    /**
     * Check if this board equal with another one
     *
     * @param obj Object
     *
     * @return boolean**/
    @Override
    public boolean equals(Object obj) {
        Board toCompareBoard = (Board) obj;
        boolean sameStonePlace =  this.boardService.getCellOfStone(this).getXIndex() == toCompareBoard.boardService.getCellOfStone(toCompareBoard).getXIndex()
                && this.boardService.getCellOfStone(this).getYIndex() == toCompareBoard.boardService.getCellOfStone(toCompareBoard).getYIndex();
        ComputerPlayer player = (ComputerPlayer) Game.getGame().getPlayer();
        if (player.getStrategy() instanceof DFSStrategy){
            return sameStonePlace;
        }
        return sameStonePlace && this.pathToBoard.containsAll(toCompareBoard.pathToBoard);
    }

    /**
     * Clone a new board object
     *
     * @return Board
     **/
    public Object clone(){
        Board board = null;
        try {
            board =  (Board) super.clone();
            // Clone cells
            LinkedList<Cell> cells = new LinkedList<>();
            for (Cell cell: this.cells
            ) {
                cells.add((Cell)cell.clone());
            }
            board.setCells(cells);
        }catch (CloneNotSupportedException exception){
            return board;
        }
        return board;
    }

}
