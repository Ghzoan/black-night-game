package backend.ground.board.models;


import backend.ground.cell.models.Cell;
import backend.ground.cell.models.ECellType;

import java.util.LinkedList;
import java.util.List;

/** This class will be define easy level board initializer method
 *
 * @author Ghzoan Safi
 **/
public class EasyLevelBoardInitializer extends BoardInitializer {

    /**
     * Class constructor
     *
     **/
    public EasyLevelBoardInitializer(){
        this.boardWidth = 6;
        this.boardHeight = 6;
    }

    /**
     * Initialize board method, this will be an abstract method, each child must be declare this method.
     *
     * @return Board
     **/
    @Override
    public Board Initialize() {
        // Initialize board
        Board board = new Board();
        // Set width and height for board
        board.setWidth(this.boardWidth);
        board.setHeight(this.boardHeight);
        // Define cell list
        LinkedList<Cell> cells = new LinkedList<>();
        // Init cells
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 0, 0, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 0, 1, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 0, 2, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 1, 0, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 1, 1, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 1, 2, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 2, 0, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 2, 1, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 2, 2, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 3, 3, false));
        cells.add(this.cellFactory.initCell(ECellType.BLOCKED_CELL, 4, 4, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 0, 3, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 0, 4, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 0, 5, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 1, 3, true));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 1, 4, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 1, 5, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 2, 3, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 2, 4, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 2, 5, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 3, 0, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 3, 1, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 3, 2, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 3, 4, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 3, 5, false));
        Cell stoneCell = this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 4, 0, false);
        stoneCell.setHaveStone(true);
        cells.add(stoneCell);
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 4, 1, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 4, 2, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 4, 3, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 4, 5, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 5, 0, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 5, 1, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 5, 2, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 5, 3, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 5, 4, false));
        cells.add(this.cellFactory.initCell(ECellType.AVAILABLE_CELL, 5, 5, false));
        // Set cells to board
        board.setCells(cells);
        return board;
    }
}
