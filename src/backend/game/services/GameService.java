package backend.game.services;

import backend.game.models.Game;
import backend.ground.board.models.Board;
import backend.ground.board.services.BoardService;
import backend.ground.cell.models.AvailableCell;
import backend.ground.cell.models.Cell;


/** This class will be define methods applied on game
 *
 * @author Ghzoan Safi
 **/
public class GameService {

    /** Board service instance **/
    private final BoardService boardService = new BoardService();

    /** Check player win after each move
     *
     * @param board Board
     *
     * @return boolean
     **/
    public boolean checkPlayerWin(Board board){
        return this.boardService.checkIfPlayerWin(board);
    }

    /**
     * Get cell of stone
     *
     * @param board Board
     *
     * @return Cell
     **/
    public Cell getCellOfStone(Board board){
        return this.boardService.getCellOfStone(board);
    }

    /**
     * Get cell of goal
     *
     * @param board Board
     *
     * @return Cell
     **/
    public Cell getCellOfGoal(Board board){
        return this.boardService.getCellOfGoal(board);
    }

    /**
     * Check road between tow cells
     *
     * @param srcCell Cell
     * @param targetCell Cell
     *
     * @return boolean
     **/
    public boolean checkRoadBetweenTowCell(Cell srcCell, Cell targetCell){
        // Get game board
        Board gameBoard = Game.getGame().getGameBoard();
        // Generate x range
        int startXRange = srcCell.getXIndex();
        int endXRange = targetCell.getXIndex();
        if (targetCell.getXIndex() < srcCell.getXIndex()){
            startXRange = targetCell.getXIndex();
            endXRange = srcCell.getXIndex();
        }
        // Generate y range
        int startYRange = srcCell.getYIndex();
        int endYRange = targetCell.getYIndex();
        if (targetCell.getYIndex() < srcCell.getYIndex()){
            startYRange = targetCell.getYIndex();
            endYRange = srcCell.getYIndex();
        }
        boolean[] roadResults = new boolean[(endXRange - startXRange) + 1];
        int roadResultsCounter = 0;
        // Check roads
        for (int i = startXRange; i <= endXRange ; i++) {
            for (int j = startYRange; j <= endYRange ; j++) {
                Cell inRangeCell = this.boardService.getCellByIndexes(gameBoard, i, j);
                if (!(inRangeCell instanceof AvailableCell)){
                   roadResults[roadResultsCounter] = false;
                   break;
                }
                roadResults[roadResultsCounter] = true;
            }
            roadResultsCounter++;
        }
        for (boolean res: roadResults) {
            if (res){
                return true;
            }
        }
        return false;
    }

    /**
     * Check cells without ranges
     *
     * @param srcCell Cell
     * @param targetCell Cell
     *
     * @return boolean
    **/
    public boolean checkCustomCells(Cell srcCell, Cell targetCell){
        if (srcCell.getXIndex() == 5 && srcCell.getYIndex() == 3 && targetCell.getXIndex() == 3 && targetCell.getYIndex() == 4){
            return false;
        }
        if (srcCell.getXIndex() == 4 && srcCell.getYIndex() == 3 && targetCell.getXIndex() == 2 && targetCell.getYIndex() == 4){
            return false;
        }
        return true;
    }
}
