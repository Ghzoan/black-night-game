package backend.game.models;

/** This enum to describe all game levels
 *
 * @author Ghzoan Safi
 **/
public enum EGameLevel {
    EASY_LEVEL,
    MEDIUM_LEVEL,
    HARD_LEVEL
}
