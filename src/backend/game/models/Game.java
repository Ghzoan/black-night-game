package backend.game.models;

import backend.game.services.GameService;
import backend.ground.board.models.Board;
import backend.ground.board.services.BoardInitializerFactory;
import backend.ground.cell.models.AvailableCell;
import backend.ground.cell.models.Cell;
import backend.player.models.EPlayerType;
import backend.player.models.Player;
import backend.stone.models.EStoneType;
import backend.stone.models.Stone;
import backend.stone.services.StoneFactory;

import java.util.LinkedList;
import java.util.List;

/** This class will be define black knight game
 *
 * This class build with singleton pattern
 *
 * @author Ghzoan Safi
 **/
public class Game {

    /** Game level **/
    private EGameLevel gameLevel;

    /** Board of game **/
    private Board gameBoard;

    /** Stone of game **/
    private Stone stone;

    /** Player type **/
    private EPlayerType playerType;

    /** Player **/
    private Player player;

    /** Player movements in all game **/
    private LinkedList<Board> playerMovements;

    /** Single game instance from this class **/
    private static Game game;

    /** Cost **/
    public int gameCost = 0;

    /** Game service instance **/
    private final GameService gameService = new GameService();


    /**
     * Class constructor
     *
     * @param gameLevel EGameLevel
     * @param player Player
     * @param stoneType EStoneType
     **/
    private Game(EGameLevel gameLevel, Player player, EStoneType stoneType){
        this.gameLevel = gameLevel;
        this.playerMovements = new LinkedList<>();
        // Initialize player from player type
        this.player = player;
        // Initialize game board from game level
        this.gameBoard = (new BoardInitializerFactory()).initBoardInitializer(gameLevel).Initialize();
        // Initialize stone from stone type
        this.stone = (new StoneFactory()).initStone(stoneType);
    }

    /**
     * Get game level
     *
     * @return EGameLevel
     **/
    public EGameLevel getGameLevel() {
        return gameLevel;
    }

    /**
     * Set game level
     *
     * @param gameLevel EGameLevel
     *
     * @return void
     **/
    public void setGameLevel(EGameLevel gameLevel) {
        this.gameLevel = gameLevel;
    }

    /**
     * Get game board
     *
     * @return Board
     **/
    public Board getGameBoard() {
        return gameBoard;
    }

    /**
     * Set game board
     *
     * @param gameBoard Board
     *
     * @return void
     **/
    public void setGameBoard(Board gameBoard) {
        this.gameBoard = gameBoard;
    }

    /**
     * Get player type
     *
     * @return EPlayerType
     **/
    public EPlayerType getPlayerType() {
        return playerType;
    }

    /**
     * Set player type
     *
     * @param playerType EPlayerType
     *
     * @return void
     **/
    public void setPlayerType(EPlayerType playerType) {
        this.playerType = playerType;
    }

    /**
     * Get player
     *
     * @return Player
     **/
    public Player getPlayer() {
        return player;
    }

    /**
     * Set player
     *
     * @param player Player
     *
     * @return void
     **/
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Get stone
     *
     * @return Stone
     **/
    public Stone getStone() {
        return stone;
    }

    /**
     * Set stone
     *
     * @param stone Stone
     *
     * @return void
     **/
    public void setStone(Stone stone) {
        this.stone = stone;
    }

    /**
     * Get player movements
     *
     * @return LinkedList
     **/
    public LinkedList<Board> getPlayerMovements() {
        return playerMovements;
    }

    /**
     * Set player movements
     *
     * @param playerMovements LinkedList<Board>
     *
     * @return void
     **/
    public void setPlayerMovements(LinkedList<Board> playerMovements) {
        this.playerMovements = playerMovements;
    }

    /**
     * Init game
     *
     * @param gameLevel EGameLevel
     * @param player Player
     * @param stoneType EStoneType
     *
     * @return void
     **/
    public static void initGame(EGameLevel gameLevel, Player player, EStoneType stoneType){
        Game.game = new Game(gameLevel, player, stoneType);
    }

    /**
     * Get game instance
     *
     * @return Game
     **/
    public static Game getGame(){
        return Game.game;
    }

    /** Get available move for player
     *
     * @return LinkedList<Board>
     **/
    public LinkedList<Board> getAvailableMovements(){
        LinkedList<Board> availableBoards = new LinkedList<>();
        // Get cell of stone
        Cell stoneCell = this.gameService.getCellOfStone(this.gameBoard);

        List<Cell>gameCells = this.gameBoard.getCells();
        // Get all possible move by stone
        for (Cell cell: gameCells
             ) {
            if (this.stone.canMoveTo(stoneCell.getXIndex(), stoneCell.getYIndex(), cell.getXIndex(), cell.getYIndex())
                    && cell instanceof AvailableCell
                    && this.gameService.checkRoadBetweenTowCell(stoneCell, cell)
                    && this.gameService.checkCustomCells(stoneCell, cell)
            ){
                // Clone a new board with this movement.
                Board clonedBoard = (Board) this.gameBoard.clone();
                clonedBoard.moveStoneTo(cell.getXIndex(), cell.getYIndex());
                availableBoards.add(clonedBoard);
            }
        }
        return availableBoards;
    }

    /** Apply player move
     *
     * @param board Board
     *
     * @return void
     **/
    public void applyPlayerMove(Board board){
        // Make current board on this move
        this.gameBoard = board;
        // Add this move to player movement
        this.playerMovements.add(board);
        // Increase game cost
        this.gameCost += 1;
    }


    /** Check player win after each move
     *
     * @return boolean
     **/
    public boolean checkPlayerWin(){
        return this.gameService.checkPlayerWin(this.gameBoard);
    }

}
