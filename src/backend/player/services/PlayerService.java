package backend.player.services;

import backend.player.models.EPlayerType;
import backend.player.models.Player;
import backend.player.models.computer.ComputerPlayer;
import backend.player.models.human.ConsolePlayer;
import backend.player.models.human.GUIPlayer;

/** This class will be define methods applied on player
 *
 * @author Ghzoan Safi
 **/
public class PlayerService {

    /**
     * Init player by type
     *
     * @param playerType EPlayerType
     *
     * @return Player
     **/
    public Player initPlayer(EPlayerType playerType){
        switch (playerType){
            case CONSOLE_PLAYER:
                return new ConsolePlayer();
            case GUI_PLAYER:
                return new GUIPlayer();
            case COMPUTER_PLAYER:
                return new ComputerPlayer();
            default:
                return null;
        }
    }

    /**
     * Get type of player
     *
     * @param player Player
     *
     * @return EPlayerType
     **/
    public EPlayerType getTypeOfPlayer(Player player){
        if (player instanceof ConsolePlayer){
            return EPlayerType.CONSOLE_PLAYER;
        }
        if (player instanceof GUIPlayer){
            return EPlayerType.GUI_PLAYER;
        }
        if (player instanceof ComputerPlayer){
            return EPlayerType.COMPUTER_PLAYER;
        }
        return null;
    }


}
