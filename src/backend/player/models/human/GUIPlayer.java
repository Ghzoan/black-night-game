package backend.player.models.human;

/** This class will be define gui player structure
 *
 * @author Ghzoan Safi
 **/
public class GUIPlayer extends HumanPlayer {
    /**
     * Method will be define player logic on each type in player
     *
     * @return void
     **/
    @Override
    public void play() {

    }
}
