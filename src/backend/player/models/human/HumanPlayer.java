package backend.player.models.human;

import backend.player.models.Player;

/** This class will be define human player structure
 *
 * @author Ghzoan Safi
 **/
public abstract class HumanPlayer extends Player {

}
