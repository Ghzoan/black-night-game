package backend.player.models.human;

/** This class will be define console player structure
 *
 * @author Ghzoan Safi
 **/
public class ConsolePlayer extends HumanPlayer {
    /**
     * Method will be define player logic on each type in player
     *
     * @return void
     **/
    @Override
    public void play() {

    }
}
