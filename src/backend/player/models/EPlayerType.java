package backend.player.models;

/** This enum to describe all player types
 *
 * @author Ghzoan Safi
 **/
public enum EPlayerType {
    CONSOLE_PLAYER,
    GUI_PLAYER,
    COMPUTER_PLAYER
}
