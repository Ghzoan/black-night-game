package backend.player.models;

/** This class will be define player structure
 *
 * @author Ghzoan Safi
 **/
public abstract class Player {
    /**
     * Method will be define player logic on each type in player
     *
     * @return void
     **/
    public abstract void play();
}
