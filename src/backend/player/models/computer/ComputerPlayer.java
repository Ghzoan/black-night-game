package backend.player.models.computer;

import backend.player.models.Player;
import backend.strategy.Strategy;

/** This class will be define computer player structure
 *
 * @author Ghzoan Safi
 **/
public class ComputerPlayer extends Player {

    /** Define player strategy **/
    private Strategy strategy;

    /**
     * Init strategy
     *
     * @param strategy Strategy
     *
     * @return void
     **/
    public void initStrategy(Strategy strategy){
        this.strategy = strategy;
    }


    public Strategy getStrategy() {
        return strategy;
    }

    /**
     * Method will be define player logic on each type in player
     *
     * @return void
     **/
    @Override
    public void play() {
        // Check if strategy initialized
        if (strategy == null){
            return;
        }
        // Apply strategy
        this.strategy.applyStrategy();
    }
}
